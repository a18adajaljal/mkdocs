<table style="width:100%">
  <tr>
    <th>Assigantures DAW 19/20</th>
    <th>UF</th>
  </tr>
  <tr>
    <td>M3 - Programació</td>
    <td>UF4 / UF5 / UF6</td>
  </tr>
  <tr>
    <td>M5 - Entorns desenvolupament</td>
    <td>UF1 / UF2 / UF3</td>
  </tr>
  <tr>
    <td>M6 - Desenvolupament Web Client</td>
    <td>UF1 / UF2 / UF3 / UF4</td>
  </tr>
  <tr>
    <td>M7 - Desenvolupament Web Servidor</td>
    <td>UF1 / UF2 / UF3 / UF4</td>
  </tr>
  <tr>
    <td>M8 - Desplegament d'aplicacions web</td>
    <td>UF1 / UF2 / UF3 / UF4</td>
  </tr>
  <tr>
    <td>M9 - Disseny D'interfícies web</td>
    <td>UF1 / UF2 / UF3</td>
  </tr>
</table>
